# GSoC - Click to Dial Popup project

### What is it ?

Git repository for debian's GSoC project "Click to Dial Popup"

## What's needed ?

You'll need PyQt4, can be installed with : 
```sh
sudo apt-get install python-qt4
```
python-phonenumbers a Python port of Google's phonenumbers is also needed, can be installed with :
```sh
pip install phonenumbers 
```
or with : 
```sh
pip3 install phonenumbers 
```
for Python3 users

## How to use it ?

Replace XXXX by a phone number
```sh
	python main.py XXXX
```
or 
```sh
	python3 main.py XXXX
```