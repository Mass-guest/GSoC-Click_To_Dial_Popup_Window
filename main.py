import sys , getopt , phonenumbers , dial_window as DW
import phonenumbers.geocoder as geocoder


# For now return first region_code that match the "is_valid" property from Goggle's phonenumbers lib
# or (if )
def get_potential_phone_number(phone_number):
	#AVAILABLE REGION CODES for now taken from https://github.com/daviddrysdale/python-phonenumbers/blob/dev/python/phonenumbers/shortdata/__init__.py
	_AVAILABLE_REGION_CODES = ['AC','AD','AE','AF','AG','AI','AL','AM','AO','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GR','GT','GU','GW','GY','HK','HN','HR','HT','HU','ID','IE','IL','IM','IN','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','ST','SV','SX','SY','SZ','TC','TD','TG','TH','TJ','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','XK','YE','YT','ZA','ZM','ZW']
	potential_number = []
	for region_code in _AVAILABLE_REGION_CODES:
		try :
			phone = phonenumbers.parse(phone_number,region_code)
			if (phonenumbers.is_valid_number(phone)):
				potential_number.append(phone) 
		except Exception:
			pass
	return potential_number


def get_phone_number(text):
	if (text[0] == '+'):
		phone_number = phonenumbers.parse(text,None)
		if (phonenumbers.is_valid_number(phone_number)):
			return [phone_number]
		else :
			raise phonenumbers.phonenumberutil.NumberParseException("Missing or invalid default region.")
	return get_potential_phone_number(text);


def main():
	try:
		phone_number = get_phone_number(sys.argv[1])
	except Exception as e:
		print("Phone number invalid")
		exit()
	window = DW.DialWindow(phone_number)


if __name__ == '__main__':
	main()
