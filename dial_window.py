
import sys
from PyQt4.QtGui import *
import phonenumbers.geocoder as geocoder

class DialWindow:
	def __init__(self,phone_number):
		self.app = QApplication(sys.argv)
		self.widget = QWidget()
		self.width = 700
		self.height = 400
		self.name = "Dial Window"
		self.phone_number = phone_number
		self.init_all()

	def init_main_window(self):
		self.widget.resize(self.width,self.height)
		self.widget.setWindowTitle(self.name)
		self.widget.show()

	def init_phone_infos(self):
		text_infos = ""
		for number in self.phone_number :
			text_infos += str(number)
			text_infos += " | Country name : "+geocoder.description_for_valid_number(number,"en")+"\n"
		label = QLabel(self.widget)
		label.setText(text_infos)
		label.move(0,0);


	def init_all(self):
		self.init_phone_infos()
		self.init_main_window()
		sys.exit(self.app.exec_())